﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CertificateManager
{
    public enum LOGTYPES            //Tipovi logova
    {
        WinEventLog,
        Txt,
        Xml
    }

    public class CommonObjects
    {
        public static string txtPath = "../../Logs/TxtLog.txt";     //Adresa za upis logova u txt
        public static string xmlPath = "../../Logs/XmlLog.xml";     //Adresa za upis logova u xml
        public static LOGTYPES type;                                //Tip loga
        public static ISysService sysFactory;                       //Definisanje sysFactory da bismo mogli da ga koristimo van metode u kojoj se inicijalizuje
       // public static int counter = 0;
        public static string userName = String.Empty;               

        

    }


}
