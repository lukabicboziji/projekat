﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CertificateManager
{
    public class XMLLoger
    {
        public static void AuthenticationSuccess(string user)                   //Upis loga uspesne autentifikacije u xml fajl
        {
            
            try
            {
                StringBuilder sbuilder = new StringBuilder();
                using (StringWriter sw = new StringWriter(sbuilder))
                {
                    using (XmlTextWriter w = new XmlTextWriter(sw))
                    {
                        w.WriteStartElement("Log");                                   // <Log>
                        w.WriteStartElement("User");                                  // <User>
                        w.WriteString(EventLogEntryType.Information.ToString());      //      Ime Usera bi trebalo
                        w.WriteEndElement();                                          // </User>
                        w.WriteStartElement("Message");                               // <Message>
                        w.WriteString("Succesfully authenticated");                   //      Succesfully autheticated
                        w.WriteEndElement();                                          // </Message>
                        w.WriteEndElement();
                     
                    }
                }

                using (StreamWriter w = new StreamWriter(CommonObjects.xmlPath, true, Encoding.UTF8))
                {
                    w.WriteLine(sbuilder.ToString());
                    
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Pukao na autentifikacija uspesna logovanju");
                throw new SystemException("PUKOSI");
            }


        }

        public static void LogAuthenticationFailure(string user, string reason)         //Upis loga neuspesne autentifikacije u xml fajl
        {

            try
            {
                StringBuilder sbuilder = new StringBuilder();
                using (StringWriter sw = new StringWriter(sbuilder))
                {
                    using (XmlTextWriter w = new XmlTextWriter(sw))
                    {
                        w.WriteStartElement("Log");                                   // <Log>
                        w.WriteStartElement("User");                                  // <User>
                        w.WriteString(EventLogEntryType.Information.ToString());      //      Ime Usera bi trebalo
                        w.WriteEndElement();                                          // </User>
                        w.WriteStartElement("Message");                               // <Message>
                        w.WriteString("unsuccesfully authenticated");                   //      Succesfully autheticated
                        w.WriteEndElement();                                          // </Message>
                        w.WriteEndElement();

                    }
                }

                using (StreamWriter w = new StreamWriter(CommonObjects.xmlPath, true, Encoding.UTF8))
                {
                    w.WriteLine(sbuilder.ToString());

                }
            }
            catch (Exception)
            {
                Console.WriteLine("Pukao na autentifikacija uspesna logovanju");
                throw new SystemException("PUKOSI");
            }



        }

        public static void FunctionSuccsess(string user, string function, string file)          //Upis loga uspesne funkcije u xml fajl
        {


            try
            {
                StringBuilder sbuilder = new StringBuilder();
                using (StringWriter sw = new StringWriter(sbuilder))
                {
                    using (XmlTextWriter w = new XmlTextWriter(sw))
                    {
                        w.WriteStartElement("Log");                                   // <Log>
                        w.WriteStartElement("User");                                  // <User>
                        w.WriteString(EventLogEntryType.Information.ToString());      //      Ime Usera bi trebalo
                        w.WriteEndElement();                                          // </User>
                        w.WriteStartElement("Message");                               // <Message>
                        w.WriteString("Successfuly called a function: " + function);  //      Successfuly called a function: nameOfFunction
                        w.WriteEndElement();                                          // </Message>
                        w.WriteStartElement("File");                                  // <File>
                        w.WriteString(file);                                          //      File path
                        w.WriteEndElement();                                          // </File>
                        w.WriteEndElement();                                          // </Log>

                    }
                }

                using (StreamWriter w = new StreamWriter(CommonObjects.xmlPath, true, Encoding.UTF8))
                {
                    w.WriteLine(sbuilder.ToString());

                }
            }
            catch (Exception)
            {
                throw new Exception();
            }


        }

    

        public static void FunctionUnsuccsess(string user, string function, string file)            //Upis loga neuspesne funkcije u xml fajl
        {


            try
            {
                StringBuilder sbuilder = new StringBuilder();
                using (StringWriter sw = new StringWriter(sbuilder))
                {
                    using (XmlTextWriter w = new XmlTextWriter(sw))
                    {
                        w.WriteStartElement("Log");                                   // <Log>
                        w.WriteStartElement("User");                                  // <User>
                        w.WriteString(EventLogEntryType.Information.ToString());      //      Ime Usera bi trebalo
                        w.WriteEndElement();                                          // </User>
                        w.WriteStartElement("Message");                               // <Message>
                        w.WriteString("UnSuccessfuly called a function: " + function);  //      Successfuly called a function: nameOfFunction
                        w.WriteEndElement();                                          // </Message>
                        w.WriteStartElement("File");                                  // <File>
                        w.WriteString(file);                                          //      File path
                        w.WriteEndElement();                                          // </File>
                        w.WriteEndElement();                                          // </Log>

                    }
                }

                using (StreamWriter w = new StreamWriter(CommonObjects.xmlPath, true, Encoding.UTF8))
                {
                    w.WriteLine(sbuilder.ToString());

                }
            }
            catch (Exception)
            {
                throw new Exception();
            }



        }
    }
}
