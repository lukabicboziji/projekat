﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Selectors;

namespace CertificateManager
{
    public class ServiceCertValidator : X509CertificateValidator
    {
        /// <summary>
        /// Implementation of a custom certificate validation on the service side.
        /// Service should consider certificate valid if its issuer is the same as the issuer of the service.
        /// If validation fails, throw an exception with an adequate message.
        /// </summary>
        /// <param name="certificate"> certificate to be validate </param>
        public override void Validate(X509Certificate2 certificate)
        {
            /// This will take service's certificate from storage
            X509Certificate2 srvCert = CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, Formatter.ParseName(WindowsIdentity.GetCurrent().Name));

            string[] temp = certificate.Subject.Split('=');
            CommonObjects.userName = temp[1];
         

             if (!certificate.Issuer.Equals(srvCert.Issuer))                                                                      //Provera da li je isti CA za oba izdata sertifikata
            {
                Loger.LogAuthFailFunc(CommonObjects.type, certificate.Subject, "Certificate has a difierent CA.",false);          //poziv funkcije sa switchem
                Console.WriteLine("Authentification failed.Certificate has a difierent CA.");                                                                                                              

                throw new Exception("Certificate is not from the valid issuer.");
            }
            else if (!certificate.Subject.StartsWith("CN=FSM_client_"))                                                             //Provera da li korisnik ima sertifikat u navedenom formatu
            {
                Loger.LogAuthFailFunc(CommonObjects.type, CommonObjects.userName, "Invalid certificate name.",false);                //poziv funkcije sa switchem
                Console.WriteLine("Authentification failed.Invalid certificate name.");
                throw new Exception("Certificate is not in valid format.");

            }
            else
            {
                Console.WriteLine("Authentification succeeded.");
                Loger.LogAuthSuccesFunc(CommonObjects.type, CommonObjects.userName,false);                                           //Ukoliko nema greaka, logovanje je uspesno
                
                
            }
        }
    }
}
