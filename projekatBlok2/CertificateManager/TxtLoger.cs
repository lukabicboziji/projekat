﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CertificateManager
{
    public class TxtLoger
    {
         public TxtLoger() { }

        public static void AuthenticationSuccess(string user)                               //Upis loga uspesne autentifikacije u txt fajl
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(CommonObjects.txtPath, true))
            {
                file.WriteLine("\n" + String.Format($"User {user} is successfully authenticated.", EventLogEntryType.Information));
                                                                                                                                  
            }
            
        }

        public static void LogAuthenticationFailure(string user, string reason)             //Upis loga neuspesne autentifikacije u txt fajl
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(CommonObjects.txtPath, true))
            {
                file.WriteLine("\n" + String.Format($"User {user} is failed to authenticated. reason: {reason}", EventLogEntryType.Information));

            }

        }

        public static void FunctionSuccsess(string user, string function, string file)        //Upis loga uspesne funkcije u txt fajl
        {

            using (System.IO.StreamWriter file1 = new System.IO.StreamWriter(CommonObjects.txtPath, true))
            {
                file1.WriteLine("\n" + string.Format("User {0} succsessfully called {1} on {2}.", user, function, file));

            }
        
        }

        public static void FunctionUnsuccsess(string user, string function, string file)        //Upis loga neuspesne funkcije u txt fajl
        {
             using (System.IO.StreamWriter file1 = new System.IO.StreamWriter(CommonObjects.txtPath, true))
            {
                file1.WriteLine("\n" + string.Format("User {0} unsuccsessfully called {1} on {2}.", user, function, file));

            }
           // File.WriteAllText(CommonObjects.txtPath, string.Format("User {0} unsuccsessfully called {1} on {2}.", user, function, file));
            
        }
    }
}
