﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CertificateManager
{
    public class Loger
    {
        public static void LogAuthSuccesFunc(LOGTYPES type, string user, bool critical)  //funkcija za ispis logovanja uspesne autentifikacije i slanje loga serveru
        {
            try
            {
                CommonObjects.sysFactory.SendToServer(String.Format($"User {user} is successfully authenticated."), critical);
            }
            catch(System.ServiceModel.Security.SecurityAccessDeniedException)
            {
                Console.WriteLine("Missing Connect permision");
            }
            catch (FaultException)
            {
                Console.WriteLine("Missing Detect permision");
            }
            catch(Exception)
            {
                Console.WriteLine("Connection with SysService lost ");
            }


            switch (type)
            {
                case LOGTYPES.WinEventLog:
                    MyAuditBehavior.AuthenticationSuccess(user);
                    break;
                case LOGTYPES.Txt:
                    TxtLoger.AuthenticationSuccess(user);
                    break;
                case LOGTYPES.Xml:
                    XMLLoger.AuthenticationSuccess(user);
                    break;
            }

        }

        public static void LogAuthFailFunc(LOGTYPES type, string user, string reason, bool critical)  //funkcija za ispis logovanja neuspesne autentifikacije i slanje loga serveru
        {
            try
            {
                CommonObjects.sysFactory.SendToServer(String.Format($"User {user} is failed to authenticate."), critical);
            }
            catch (System.ServiceModel.Security.SecurityAccessDeniedException)
            {
                Console.WriteLine("Missing Connect permision");
            }
            catch (FaultException)
            {
                Console.WriteLine("Missing Detect permision");
            }
            catch (Exception)
            {
                Console.WriteLine("Connection with SysService lost");
            }

            switch (type)
            {
                case LOGTYPES.WinEventLog:
                    MyAuditBehavior.LogAuthenticationFailure(user, reason);
                    break;
                case LOGTYPES.Txt:
                    TxtLoger.LogAuthenticationFailure(user, reason);
                    break;
                case LOGTYPES.Xml:
                    XMLLoger.LogAuthenticationFailure(user, reason);
                    break;
                default:
                    break;
            }
        }

        public static void LogFuncSucces(LOGTYPES type, string user, string function, string file, bool critical)  //funkcija za ispis logovanja uspesnog poziva funkcije i slanje loga serveru
        {
            try
            {
                CommonObjects.sysFactory.SendToServer(String.Format("User {0} ~successfully~ called {1}  on ~{2} ", user, function, file), critical);
            }
            catch (System.ServiceModel.Security.SecurityAccessDeniedException)
            {
                Console.WriteLine("Missing Connect permision");
            }
            catch (FaultException)
            {
                Console.WriteLine("Missing Detect permision");
            }
            catch (Exception)
            {
                Console.WriteLine("Connection with SysService lost");
            }

            switch (type)
            {
                case LOGTYPES.WinEventLog:
                    MyAuditBehavior.FunctionSuccsess(user, function, file);
                    break;
                case LOGTYPES.Txt:
                    TxtLoger.FunctionSuccsess(user, function, file);
                    break;
                case LOGTYPES.Xml:
                    XMLLoger.FunctionSuccsess(user, function, file);
                    break;
                default:
                    break;
            }
        }

        public static void LogFuncFail(LOGTYPES type, string user, string function, string file,bool critical) //funkcija za ispis logovanja neuspesnog poziva funkcije i slanje loga serveru
        {
            try
            {
                CommonObjects.sysFactory.SendToServer(String.Format("User {0} ~unsuccessfully~ called {1} on ~{2} ", user, function, file), critical);
            }
            catch (System.ServiceModel.Security.SecurityAccessDeniedException)
            {
                Console.WriteLine("Missing Connect permision");
            }
            catch (FaultException)
            {
                Console.WriteLine("Missing Detect permision");
            }
            catch (Exception)
            {
                Console.WriteLine("Connection with SysService lost");
            }


            switch (type)
            {
                case LOGTYPES.WinEventLog:
                    MyAuditBehavior.FunctionUnsuccsess(user, function, file);
                    break;
                case LOGTYPES.Txt:
                    TxtLoger.FunctionUnsuccsess(user, function, file);
                    break;
                case LOGTYPES.Xml:
                    XMLLoger.FunctionUnsuccsess(user, function, file);
                    break;
                default:
                    break;
            }
        }
    }
}
