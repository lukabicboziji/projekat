﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CertificateManager
{
    public sealed class MyAuditBehavior 
    {

         static MyAuditBehavior()
        {
            if (!EventLog.SourceExists("FSM"))                                                  //Kreiranje logera "FSMLoguje" ukoliko ne postoji
            {
                EventLog.CreateEventSource("FSM", "FSMLoguje");
            }
            MyLogger = new EventLog("FSMLoguje", Environment.MachineName, "FSM");
        }

        private static EventLog MyLogger { get; set; }

        public static void AuthenticationSuccess(string user)       //Ispis u loger da je autentifikacija uspesna
        {
            MyLogger.WriteEntry($"User {user} is successfully authenticated.", EventLogEntryType.Information);
        }

        public static void LogAuthenticationFailure(string user, string reason)     //Ispis u loger da je autentifikacija neuspesna
        {
            MyLogger.WriteEntry(string.Format("User {0} failed to authenticate. Reason: {1}.", user, reason));
        }

        public static void FunctionSuccsess(string user, string function, string file)     //Ispis u loger da je funkcija uspesno izvrsena
        {
            MyLogger.WriteEntry(string.Format("User {0} succsessfully called {1} on {2}.", user, function, file ));
        }
        public static void FunctionUnsuccsess(string user, string function, string file)    //Ispis u loger da je funkcija neuspesno izvrsena
        {
            MyLogger.WriteEntry(string.Format("User {0} unsuccsessfully called {1} on {2}.", user, function, file));
        }

    }
}
