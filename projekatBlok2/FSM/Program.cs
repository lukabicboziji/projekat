﻿using CertificateManager;
using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace FSM
{
    public class Program
    {
        static void Main(string[] args)
        {
            //MyAuditBehavior.FunctionSuccsess("", "Create Event Log", "");                 //   Poziva se sa administratorom samo prvi put (da se kreira folder u Event Logu)
           // Console.WriteLine("FSMLoguje succesfully created in Event Log");

            string IP_ADDRESS = "localhost";                                             // IP adresa SysLog-a


            string name = Environment.UserName;                                             //Ime korisnika koji je pozvao program
            string port = "";       
            switch (name)                                                                   //Na osnovu korisnika odredjuje se port
            {
                case "FSM":
                    port = "9997";
                    break;
                case "FSM1":
                    port = "9996";
                    break;
                case "FSM2":
                    port = "9995";
                    break;
                case "FSM3":
                    port = "9994";
                    break;
                case "FSM4":
                    port = "9993";
                    break;
                case "FSM5":
                    port = "9992";
                    break;
            }

            Console.WriteLine("User: {0} , port: {1}", name, port);                     


            SetupClient(IP_ADDRESS);                                                    //Uspostavlja se komunikacija sa serverom
          
            string srvCertCN = Formatter.ParseName(WindowsIdentity.GetCurrent().Name);      //Pravimo ime sertifikata na osnovu imena korisnika

            if (CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, srvCertCN) == null)
            {
                Console.WriteLine("FSM certificate not provided. FSM closing...");
                Console.WriteLine("Pres Enter to continue");
                Console.ReadLine();
            }
            else
            {

                NetTcpBinding binding = new NetTcpBinding();
                binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;       //Autentifikacija preko sertifikata  
                string address = "net.tcp://localhost:" + port + "/FileService";


                ServiceHost host = new ServiceHost(typeof(FileService));
                host.AddServiceEndpoint(typeof(IFileService), binding, address);

                host.Description.Behaviors.Remove(typeof(ServiceDebugBehavior));
                host.Description.Behaviors.Add(new ServiceDebugBehavior() { IncludeExceptionDetailInFaults = true });

                host.Credentials.ClientCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.Custom;     //Podesavanje hosta za autentifikaciju preko sertifikata
                host.Credentials.ClientCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;
                host.Credentials.ClientCertificate.Authentication.CustomCertificateValidator = new ServiceCertValidator();      //Postavljamo validator na CustomValidator koji smo sami napravili
                host.Credentials.ServiceCertificate.Certificate = CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, srvCertCN);  //Preuzimanje sertifikata iz skladista po imenu korisnika


                try
                {


                    Menu();
                    host.Open();

                    Console.WriteLine("FSM service is opened. Press <enter> to finish...");
                    Console.ReadLine();
                }
                catch (Exception e)
                {

                    Console.WriteLine("[ERROR] {0}", e.Message);
                    Console.WriteLine("[StackTrace] {0}", e.StackTrace);
                }
                finally
                {
                    host.Close();
                }


            }
        }

        public static void SetupClient(string IP_ADDRESS)
        {

            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.Transport;  //Transport - pravi sigurni kanal za komunikaciju
            binding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.EncryptAndSign;        //Sifrovanje poruka
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;   //Koriscenje Windows kredencijala (WindowsAutentificationProtocol)
            EndpointAddress address = new EndpointAddress(new Uri("net.tcp://" + IP_ADDRESS + ":9998/SysService"));
            ChannelFactory<ISysService> factory = new ChannelFactory<ISysService>(binding, address);        //Podizanje klijenta
            CommonObjects.sysFactory = factory.CreateChannel();     //Pozivamo preko CommonObjects da bi svi mogli da ga vise
        }

        public static void Menu()
        {
            bool check = true;

            while (check)
            {
                Console.WriteLine("Please chose log type: ");
                Console.WriteLine("1. Windows Event Log ");
                Console.WriteLine("2. Log in .txt file");
                Console.WriteLine("3. Log in .xml file");
                int pom;
                Int32.TryParse(Console.ReadLine(), out pom);

                switch (pom)                                                //Odabir nacina logovanja
                {
                    case 1:
                        CommonObjects.type = LOGTYPES.WinEventLog;
                        check = false;
                        break;
                    case 2:
                        CommonObjects.type = LOGTYPES.Txt;

                        if (!File.Exists(CommonObjects.txtPath))
                            File.Create(CommonObjects.txtPath).Dispose();
                        check = false;
                        break;
                    case 3:
                        CommonObjects.type = LOGTYPES.Xml;
                        check = false;
                        break;
                    default:
                        Console.WriteLine("Wrong enter, please try again");
                        break;
                }
            }
        }
    }
}
