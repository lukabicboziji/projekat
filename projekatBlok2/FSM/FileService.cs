﻿using CertificateManager;
using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FSM
{
    public class FileService : IFileService
    {

       
        public void Delete(string path)     //brise fajl
        {
            path = CheckPath(path);
         
            try
            {
                if (File.Exists(WriteInFolder(path)))
                {

                    File.Delete(WriteInFolder(path));
                    Console.WriteLine("Function Delete() called. File on path: {0} succesfully deleted!",path);
                    Loger.LogFuncSucces(CommonObjects.type, CommonObjects.userName, "Delete()", path, false);              //poziv funkcije sa switchem
                    
                }
                else
                {
                    Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "Delete()", path, false);                //poziv funkcije sa switchem                                                                                           
                    Console.WriteLine("Function Delete() called. File on path {0} doesn't exist. Delete failed!", path);
                    throw new Exception(String.Format("File on path {0} doesn't exist. Delete failed!",path));
                   
                    
                }

            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        public string Read(string path)     //cita podatke koji se nalaze u fajlu
        {

            path = CheckPath(path);
            string text = String.Empty;

            try
            {
                if (File.Exists(WriteInFolder(path)))
                {
                    text = System.IO.File.ReadAllText(WriteInFolder(path));
                    Loger.LogFuncSucces(CommonObjects.type, CommonObjects.userName, "Read()", path, false);                //poziv funkcije sa switchem
                    Console.WriteLine("Function Read() called successfully");

                }
                else
                {
                    text = "File doesn't exist.";
                    Console.WriteLine("Function Read() called. File doesn't exist.");
                    Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "Read()", path, false);
                }
            }
            catch (Exception)
            {
                Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "Read()", path, false);                      //poziv funkcije sa switchem              
                throw new Exception();
            }

            return text;
        }
        
        public void Write(string path, string text)         //Upisuje string u fajl (ne brise ono sto je vec prethodno bilo u fajlu)
        {

            path = CheckPath(path);

            try
            {
                if (File.Exists(WriteInFolder(path)))
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(WriteInFolder(path), true))
                    {
                        file.WriteLine("\n" + text);
                        Loger.LogFuncSucces(CommonObjects.type, CommonObjects.userName, "Write()", path, false);               //poziv funkcije sa switchem
                        Console.WriteLine("Function Write() called successfully");

                    }
                }
                else if (!File.Exists(WriteInFolder(path)))     //Kreira fajl ako ne postoji
                {
                    File.Create(path).Dispose();

                    using (TextWriter tw = new StreamWriter(WriteInFolder(path)))
                    {
                        tw.WriteLine(text);
                        Console.WriteLine("Function Write() called successfully(new file maked)");
                    }
                }
            }
            catch (Exception)
            {
                Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "Write()", path, false);                         //poziv funkcije sa switchem
                Console.WriteLine("Function Write() called unsuccessfully(Write failed)");
                throw new Exception("Write failed.");
            }
        }

        public void CreateFile(string path)             //Kreiranje fajla
        {

            path = CheckPath(path);

            try
            {
                if (!File.Exists(WriteInFolder(path)))
                {
                    File.Create(WriteInFolder(path)).Dispose();
                    Console.WriteLine("Function CreateFile() called. File on path {0} successfully created.", path);
                    Loger.LogFuncSucces(CommonObjects.type, CommonObjects.userName, "CreateFile()", path, false);              //poziv funkcije sa switchem               
                }
                else
                {
                    Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "CreateFile()", path, false);                //poziv funkcije sa switchem                    
                    Console.WriteLine("Function CreateFile() called. File allredy exists");
                }
            }
            catch (Exception)
            {
                Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "CreateFile()", path, false);                    //poziv funkcije sa switchem               
                throw new Exception();
            }
        }
        
        public bool FileExcist(string path)             //proverava da li postoji odredjeni fajl
        {
            path = CheckPath(path);

            try
            {
                if (File.Exists(WriteInFolder(path)))
                {
                    Console.WriteLine("FileExcist function called. FIle on path:{0} exist",path);
                    Loger.LogFuncSucces(CommonObjects.type, CommonObjects.userName, "FileExist()", path, false);               //poziv funkcije sa switchem
                    return true;
                }

                else
                {
                    Loger.LogFuncSucces(CommonObjects.type, CommonObjects.userName, "FileExist()", path, false);               //poziv funkcije sa switchem
                    Console.WriteLine("FileExcist function called. FIle on path:{0} doesn't exist", path);
                    return false;
                }
            }
            catch (Exception)
            {
                Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "FileExist()", path, false);                     //poziv funkcije sa switchem
                throw new Exception();
            }

        }

        public string ListFilesInFolder()                   //f-ja broji fajlove u folderu
        {
            string str = "";
            try
            {
                string folder = WriteInFolder("");
                folder.Remove(folder.Length - 1);

                DirectoryInfo d = new DirectoryInfo(folder);
                FileInfo[] Files = d.GetFiles("*.txt");
                
                Console.WriteLine("ListFilesInFolder() function called");

                foreach (FileInfo file in Files)
                {
                    str = str + "\n" + file.Name;
                }

                Loger.LogFuncSucces(CommonObjects.type, CommonObjects.userName, "ListFilesInFolder()", "Console",false);              //poziv funkcije sa switchem
            }
            catch (Exception)
            {
                Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "ListFileInFoder()", "Console", false);                  //poziv funkcije sa switchem
                throw new FaultException();
            }
            
            return str;
        }

        public bool LogSimulator(int option,string file)
        {
            try
            {
                switch (option)                                                                                         //Simulira neuspesne pozive funkcija da bismo mogli da testiramo WARNING-e
                {
                    case 1:
                        Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "Read()", file,false);
                        Console.WriteLine("Fail log sim on Read() function called");
                        return true;
                        
                    case 2:
                        Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "Write()", file,false);
                        Console.WriteLine("Fail log sim on Write() function called");
                        return true;
                    case 3:
                        Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "Create()", file,false);
                        Console.WriteLine("Fail log sim on Create() function called");
                        return true;
                    case 4:
                        Loger.LogFuncFail(CommonObjects.type, CommonObjects.userName, "Delete()", file,true);
                        Console.WriteLine("Fail log sim on Delete() function called with detect check");
                        return true;

                    default:
                        return false;
                }
            }
            catch (Exception)
            {

                throw new Exception();
            }
           
            
        }


        public string WriteInFolder(string path)            //pravi folder ako ne postoji (Files) u koji upisuje kreirane fajlove
        {                                                   //pravi relativnu putanju 
            try
            {
                string subPath = "../../Files";
                System.IO.Directory.CreateDirectory(subPath);
                return subPath + "/" + path;
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }


        public string CheckPath(string path)
        {
            string[] pom = path.Split('.');
            int pomLength = pom.Length;

            if (pomLength == 1)
            {
                path = pom[0] + ".txt";
            }

            else if (pom[1] != "txt")
            {
                throw new Exception("\nWrong extension please try with .txt files");
            }

            return path;

        }
        
    }
}