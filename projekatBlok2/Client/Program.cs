﻿using CertificateManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Program
    {
        static void Main(string[] args)
        {

            string IP_ADDRESS = "localhost";

            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;

            Console.WriteLine("Please enter FSM port ");                //Unos porta FSM-a 
            int port;
            Int32.TryParse(Console.ReadLine(), out port);


            string srvCertCN = String.Empty;                //Na osnovu porta cemo uzeti ime servera
            switch (port)
            {
                case 9997:
                    srvCertCN = "FSM";
                    break;
                case 9996:
                    srvCertCN = "FSM1";
                    break;
                case 9995:
                    srvCertCN = "FSM2";
                    break;
                case 9994:
                    srvCertCN = "FSM3";
                    break;
                case 9993:
                    srvCertCN = "FSM4";
                    break;
                case 9992:
                    srvCertCN = "FSM5";
                    break;

            }

            if (CertManager.GetCertificateFromStorage(StoreName.TrustedPeople, StoreLocation.LocalMachine, srvCertCN) == null)
            {
                Console.WriteLine("You don't have service certificate. Check trusted people setup");
                Console.WriteLine("Client closing..Press Enter to continue..");
                Console.ReadKey();
            }

            else
            {
                X509Certificate2 srvCert = CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, srvCertCN);  //Klijent uzima svoj sertifikat
                EndpointAddress address = new EndpointAddress(new Uri("net.tcp://" + IP_ADDRESS + ":" + port.ToString() + "/FileService"), new X509CertificateEndpointIdentity(CertManager.GetCertificateFromStorage(StoreName.TrustedPeople, StoreLocation.LocalMachine, srvCertCN)));  //Pravi endpoint serviera na koji se kaci uz pomoc sertifikata servera

                try
                {
                    using (WCFClient proxy = new WCFClient(binding, address))
                    {

                        string file;
                        string retVal;
                        string temp;


                        bool exitCheck = true;

                        while (exitCheck)
                        {

                            int option = Menu();
                            switch (option)
                            {
                                case 1:
                                    Console.WriteLine("Please enter the name of the file: (example or example.txt): ");
                                    file = Console.ReadLine();
                                    temp = proxy.Read(file);
                                    Console.WriteLine("Read from file: " + file);
                                    Console.WriteLine("\t" + temp);
                                    break;
                                case 2:
                                    Console.WriteLine("Please enter the name of the file: (example or example.txt):  ");
                                    file = Console.ReadLine();
                                    Console.WriteLine("Please enter text for writing in a file: {0} ", file);
                                    retVal = Console.ReadLine();
                                    proxy.Write(file, retVal);
                                    break;
                                case 3:
                                    Console.WriteLine("Please enter the name of the file: (example or example.txt): ");
                                    file = Console.ReadLine();
                                    proxy.Delete(file);
                                    break;
                                case 4:
                                    Console.WriteLine("Please enter the name of the file: (example or example.txt): ");
                                    file = Console.ReadLine();
                                    proxy.CreateFile(file);
                                    break;
                                case 5:
                                    Console.WriteLine("Please enter the name of the file: (example or example.txt): ");
                                    file = Console.ReadLine();
                                    if (proxy.FileExcist(file))
                                    {
                                        Console.WriteLine("File exist!");
                                    }
                                    else
                                    {
                                        Console.WriteLine("File doesn't exist!");
                                    }
                                    break;
                                case 6:
                                    Console.WriteLine("Files in folder:");
                                    Console.WriteLine("\t" + proxy.ListFilesInFolder());
                                    Console.WriteLine();
                                    break;

                                case 7:
                                    LogSimMenu(proxy);
                                    break;
                                case 8:
                                    try
                                    {
                                        exitCheck = false;
                                    }
                                    catch (CommunicationException)
                                    {

                                        throw new CommunicationObjectFaultedException();
                                    }
                                   
                                    break;
                                default:
                                    Console.WriteLine("Wrong enter, please use numbers 1-8");
                                    break;
                            }
                        }
                    }
                }
                catch (System.ServiceModel.CommunicationObjectFaultedException)
                {
                    Console.WriteLine("Connection lost. Press Enter to close");
                    Console.ReadLine();
                }
            }
        }

        public static void LogSimMenu(WCFClient proxy)
        {
            int option = 0;

            while (option != 5)
            {
                Console.WriteLine("Chose option:");
                Console.WriteLine("1.Read from file with failed logg");
                Console.WriteLine("2.Write in file with failed logg");
                Console.WriteLine("3.Create file with failed logg");
                Console.WriteLine("4.Delete file with failed logg(DIRECT WARNING)");
                Console.WriteLine("5.Back");
                Int32.TryParse(Console.ReadLine(), out option);
                string file = String.Empty;

                switch (option)
                {
                    case 1:
                        Console.WriteLine("Please enter file name: ");
                         file = Console.ReadLine();
                        proxy.LogSimulator(1, file);
                        break;
                    case 2:
                        Console.WriteLine("Please enter file name: ");
                         file = Console.ReadLine();
                        proxy.LogSimulator(2, file);
                        break;
                    case 3:
                        Console.WriteLine("Please enter file name: ");
                         file = Console.ReadLine();
                        proxy.LogSimulator(3, file);
                        break;
                    case 4:
                        Console.WriteLine("Please enter file name: ");
                         file = Console.ReadLine();
                        proxy.LogSimulator(4, file);
                        break;
                    default:
                        break;
                }

                Console.Clear();
            }
        }


        public static int Menu()
        {
            Console.WriteLine("Chose option:");
            Console.WriteLine("1.Read from file");
            Console.WriteLine("2.Write in file");
            Console.WriteLine("3.Delete file");
            Console.WriteLine("4.Create file");
            Console.WriteLine("5.Check existence of the file");
            Console.WriteLine("6.Show all files in folder");
            Console.WriteLine("7.Logg simulator");
            Console.WriteLine("8.Exit");
            int option;
            Int32.TryParse(Console.ReadLine(), out option);
            Console.Clear();

            return option;
        }
    }
}
