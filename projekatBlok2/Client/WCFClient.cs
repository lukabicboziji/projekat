﻿using CertificateManager;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class WCFClient : ChannelFactory<IFileService>, IFileService, IDisposable
    {
        IFileService factory;

        public WCFClient(NetTcpBinding binding, EndpointAddress address)
            : base(binding, address)
        {
            string cltCertCN = Formatter.ParseName(WindowsIdentity.GetCurrent().Name);

            this.Credentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.Custom;        //Koriscenje validacije za sertifikate
            this.Credentials.ServiceCertificate.Authentication.CustomCertificateValidator = new ClientCertValidator();      //Koriscenje nase sertifikacije
            this.Credentials.ServiceCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;     

            this.Credentials.ClientCertificate.Certificate = CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, cltCertCN);        //uzimanje klijentskog sertifikata iz skladista

            
                factory = this.CreateChannel();
            
            
        }

        public void Delete(string path)                                             //Pozivi funkcija
        {
            try
            {
                factory.Delete(path);
                Console.WriteLine("Delete() allowed.");

            }
            catch (System.InvalidOperationException)
            {
                Console.WriteLine("Error while trying to Delete(). Certificate not provided");
            }
            catch (CommunicationException)
            {

                Console.WriteLine("Error while trying to ListFilesInFolder(). Authentification failed.");
            }
            catch (Exception)
            {
                Console.WriteLine("Error while trying to Delete(). Authentification failed.");
            }
        }

        public string Read(string path)
        {

            string retVal = String.Empty;
            try
            {

                retVal = factory.Read(path);
                Console.WriteLine("Read() allowed.");


            }
            catch (FaultException)
            {

                Console.WriteLine("Error while trying to Read(). Authentification failed.");
            }
            catch (CommunicationException)
            {

                Console.WriteLine("Error while trying to ListFilesInFolder(). Authentification failed.");
            }
            catch (System.InvalidOperationException)
            {
                Console.WriteLine("Error while trying to Read(). Certificate not provided");
            }
            catch (Exception)
            {
                throw new System.ServiceModel.CommunicationObjectFaultedException();
                
            }


            return retVal;
        }

        public void Write(string path, string text)
        {
            try
            {

                factory.Write(path, text);
                Console.WriteLine("Write() allowed.");


            }
            catch (FaultException)
            {

                Console.WriteLine("Error while trying to Write(). Authentification failed.");
            }
            catch (CommunicationException)
            {

                Console.WriteLine("Error while trying to ListFilesInFolder(). Authentification failed.");
            }
            catch (System.InvalidOperationException)
            {
                Console.WriteLine("Error while trying to Write(). Certificate not provided");
            }

            catch (Exception)
            {
                throw new System.ServiceModel.CommunicationObjectFaultedException();

            }
        }

        public void CreateFile(string path)
        {
            try
            {

                factory.CreateFile(path);
                Console.WriteLine("CreateFile() allowed.");


            }
            catch (FaultException)
            {

                Console.WriteLine("Error while trying to CreateFile(). Authentification failed.");
            }
            catch (CommunicationException)
            {

                Console.WriteLine("Error while trying to ListFilesInFolder(). Authentification failed.");
            }
            catch (System.InvalidOperationException)
            {
                Console.WriteLine("Error while trying to CreateFile(). Certificate not provided");
            }
            catch (Exception)
            {
                throw new System.ServiceModel.CommunicationObjectFaultedException();

            }
        }

        public bool FileExcist(string path)
        {

            string retVal = String.Empty;
            try
            {

                if (factory.FileExcist(path))
                {
                    Console.WriteLine("FileExcist() allowed.");
                    return true;
                }
                else
                {
                    Console.WriteLine("FileExcist() in not allowed.");
                    return false;
                }

            }
            catch (FaultException)
            {

                Console.WriteLine("Error while trying to FileExcist(). Authentification failed.");
                return false;
            }
            catch (CommunicationException)
            {

                Console.WriteLine("Error while trying to ListFilesInFolder(). Authentification failed.");
                return false;
            }
            catch (System.InvalidOperationException)
            {
                Console.WriteLine("Error while trying to FileExcist(). Certificate not provided");
                return false;
            }

            catch (Exception)
            {
                throw new System.ServiceModel.CommunicationObjectFaultedException();

            }
        }

        public string ListFilesInFolder()
        {
            string retVal = String.Empty;

            try
            {

                retVal = factory.ListFilesInFolder();
                Console.WriteLine("ListFilesInFolder() allowed.");

            }
            catch (FaultException)
            {

                Console.WriteLine("Error while trying to ListFilesInFolder(). Authentification failed.");
            }

            catch (CommunicationException)
            {

                Console.WriteLine("Error while trying to ListFilesInFolder(). Authentification failed.");
            }

            catch (System.InvalidOperationException)
            {
                Console.WriteLine("Error while trying to ListFilesInFolder(). Certificate not provided");
            }
            catch (Exception)
            {
               
                throw new System.ServiceModel.CommunicationObjectFaultedException();

            }

            return retVal;
        }

        public bool LogSimulator(int option, string file)
        {
            try
            {

                if (factory.LogSimulator(option, file))
                {
                    Console.WriteLine("LogSimulator() allowed.");
                    return true;
                }
                else
                {
                    Console.WriteLine("LogSimulator() failed.");
                    return false;
                }

            }
            catch (FaultException)
            {

                Console.WriteLine("Error while trying to LogSimulator(). Authentification failed.");
                return false;
            }
            catch (CommunicationException)
            {

                Console.WriteLine("Error while trying to ListFilesInFolder(). Authentification failed.");
                return false;
            }
            catch (System.InvalidOperationException)
            {
                Console.WriteLine("Error while trying to LogSimulator(). Certificate not provided");
                return false;
            }

            catch (Exception)
            {
                throw new System.ServiceModel.CommunicationObjectFaultedException();

            }
        }
    }
}
