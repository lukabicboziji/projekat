﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [ServiceContract]
    public interface IFileService
    {
        [OperationContract]
        string Read(string path);

        [OperationContract]
        void Write(string path, string text);

        [OperationContract]
        void Delete(string path);

        [OperationContract]
        void CreateFile(string path);

        [OperationContract]
        bool FileExcist(string path);

        [OperationContract]
        string ListFilesInFolder();

        [OperationContract]
        bool LogSimulator(int option, string file);
    }
}
