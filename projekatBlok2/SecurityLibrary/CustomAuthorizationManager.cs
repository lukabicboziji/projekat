﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Security.Principal;
using System.Threading;

namespace SecurityManager
{
    public class CustomAuthorizationManager : ServiceAuthorizationManager
    {
        protected override bool CheckAccessCore(OperationContext operationContext) //Metoda se poziva pre svakog poziva SendToServer metode (proverava da li klijent ima prava poziva)
        {
            WindowsIdentity wId = operationContext.ServiceSecurityContext.WindowsIdentity;      //Uzima identitet klijenta


            var groups = wId.Groups;            // Uzimanje svih grupa u kojima se nalazi klijent

            foreach (IdentityReference group in wId.Groups)
            {
                SecurityIdentifier sid = (SecurityIdentifier)group.Translate(typeof(SecurityIdentifier));
                var name = sid.Translate(typeof(NTAccount));    //uzimanje imena grupe

                var temp = name.ToString();
                if (name.Value.Contains("\\"))
                    temp = name.ToString().Split('\\')[1];      //odvajanje imena grupe, uzimanje vrednosti sa desne strane


                if (temp == "FSM_Connect")      //Ako pripada grupi Connect, ima pravo poziva
                    return true;
                
            }
            return false;
        }

    }
}
