﻿using SecurityLibrary;
using System.Collections.Generic;
using System.Security.Principal;
using static SecurityLibrary.Permisions;

namespace SecurityManager
{
    public class CustomPrincipal : IPrincipal
    {
        private WindowsIdentity windowsIdentity;
        private HashSet<permisions> _permisions;

        public CustomPrincipal(WindowsIdentity windowsIdentity)
        {
            this.windowsIdentity = windowsIdentity;
            _permisions = new HashSet<permisions>();

            var _groups =  this.windowsIdentity.Groups;

            foreach (var item in _groups)
            {
                    SecurityIdentifier sid =(SecurityIdentifier)item.Translate(typeof(SecurityIdentifier));
                    var name = sid.Translate(typeof(NTAccount));

                    var temp = name.ToString();                                             //Za svaku grupu uzima njeno ime
                if (name.Value.Contains("\\"))
                    temp = name.ToString().Split('\\')[1];

                if (temp == "FSM_Connect")                                                  //Dodeljivanje permisija korisnicima u zavisnosti od toga u kojim se grupama nalaze
                {
                    if (!_permisions.Contains(permisions.Connect))
                        _permisions.Add(permisions.Connect);
                }

               else if (temp == "FSM_Monitor")
                {
                    if(!_permisions.Contains(permisions.Monitor))
                          _permisions.Add(permisions.Monitor);
                }
                else if (temp  == "FSM_Detect")
                {
                    if (!_permisions.Contains(permisions.Detect))
                        _permisions.Add(permisions.Detect);
                }
                else if (temp == "FSM_Admins")
                {
                 
                    if (!_permisions.Contains(permisions.Monitor))
                        _permisions.Add(permisions.Monitor);
                    if (!_permisions.Contains(permisions.Detect))
                        _permisions.Add(permisions.Detect);

                }
            }

        }

        public IIdentity Identity { get { return windowsIdentity; } }

        public bool IsInRole(string role)
        {
            
            switch (role)                                                               //U zavisnosti koju Rolu ima, dodajemo permisije
            {
                case "Connect":
                    if (_permisions.Contains(permisions.Connect))
                        return true;
                    else
                        return false;
                case "Monitor":
                    if (_permisions.Contains(permisions.Monitor))
                        return true;
                    else
                        return false;
                case "Detect":
                    if (_permisions.Contains(permisions.Detect))
                        return true;
                    else
                        return false;
                default:
                    return false;
            }
           
        }
    }
}