﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SYSLogServer
{
    public class SysCommonObjects
    {
        public static List<string> ports = new List<string>() { "9997", "9996", "9995", "9994", "9993", "9992", "9991","9990" };
        public static int counter = -1;
        public static string path = "../../SysLog.txt";
        public static int N = 60;  //vreme
        public static int M = 3;  //broj pokusaja uzastopnih neuspesnih operacija u okviru N vremena
    }
}
