﻿using CertificateManager;
using Common;
using SecurityManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Policy;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SYSLogServer
{
    public class Program
    {
        static void Main(string[] args)
        {

                                                                    //Podesavanje binding-a mora da bude isto i na serveru i na klijentu u suprotnom nece raditi
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.Transport;         //Transport - pravi sigurni kanal za komunikaciju
            binding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.EncryptAndSign;
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;

            string address = "net.tcp://localhost:9998/SysService";


            ServiceHost host = new ServiceHost(typeof(SysService));
            host.AddServiceEndpoint(typeof(ISysService), binding, address);
            host.Authorization.ServiceAuthorizationManager = new CustomAuthorizationManager();

            List<IAuthorizationPolicy> policies = new List<IAuthorizationPolicy>();         //Menjanje Default Policy-a u nas Custom Policy
            policies.Add(new CustomAuthorizationPolicy());                                  //Dodavanje CustomPolicy-a na port
            host.Authorization.ExternalAuthorizationPolicies = policies.AsReadOnly();
            host.Authorization.PrincipalPermissionMode = System.ServiceModel.Description.PrincipalPermissionMode.Custom;    //omogucava koriscenje CustomPrincipala umesto default
            try
            {
              
                host.Open();

                Console.WriteLine("SYSLogService is opened. Press <enter> to finish...");
                Console.ReadLine();
            }
            catch (Exception e)
            {

                Console.WriteLine("[ERROR] {0}", e.Message);
                Console.WriteLine("[StackTrace] {0}", e.StackTrace);
            }
            finally
            {
                host.Close();
            }


        }



        
    }
}
