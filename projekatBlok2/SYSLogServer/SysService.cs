﻿using Common;
using SecurityManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SYSLogServer
{
    public class SysService : ISysService
    {


        public void SendToServer(string log, bool critical)
        {

          
                IIdentity id = Thread.CurrentPrincipal.Identity;            //uzima IIdentiti korisnika koji je pokrenuo proces
                WindowsIdentity wID = id as WindowsIdentity;

                CustomPrincipal principal = Thread.CurrentPrincipal as CustomPrincipal;     //uzima princiapl klijenta sa treda i interpretira ga        
                string logText = String.Empty;                                              //kao custom principal da bi koristio IsInRole funckciju koju smo mi sami pravili



                if (!File.Exists(SysCommonObjects.path))     //Kreira fajl ako ne postoji
                {
                    File.Create(SysCommonObjects.path).Dispose();
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(SysCommonObjects.path, true))
                    {
                        file.WriteLine("    MSSG               TIME         FSM_USER_NAME      CLIENT_USER_NAME     SUCC/UNSUCC     FUNCTION       FILE ");
                    }
                    Console.WriteLine("SysLog file created");
                }

                if (critical)
                {

                    if (principal.IsInRole("Detect"))
                    {
                        logText = "[!!!WARNING!!!]~  " + DateTime.Now.ToString() + "~" + wID.Name + "~ " + log;

                    }
                    else
                    {
                        throw new Exception("No permision for detect");
                    }
                }

           
            
                else if (principal.IsInRole("Monitor"))
                {
                    
                      if (CheckForCritical(log, SysCommonObjects.path))
                      {

                          logText = "[!!!WARNING!!!]~  " + DateTime.Now.ToString() + "~" + wID.Name + "~ " + log;
    
                      }

                      else
                      {

                       logText = "                  " + DateTime.Now.ToString() + "~" + wID.Name + "~ " + log;

                      }
                }
                else
                {
                    logText = "                  " + DateTime.Now.ToString() + "~" + wID.Name + "~ " + log;
                }


                using (System.IO.StreamWriter file = new System.IO.StreamWriter(SysCommonObjects.path, true))
                {
                    file.WriteLine(logText);
                }
                Console.WriteLine("*New log* " + log + " added to sysLog : " + DateTime.Now.ToString());
            

        }
    


        private bool CheckForCritical(string log,string path)        //nama vazna funkcija, argument je onaj log sto salje FSM.. ja bih je pozivao u SysLogServer.SendToServer
        {
            string[] splitLog;                                                                          //splituje log koji nam je poslao FSM (User {0} ~successfully~ blablabla on~ primer.txt)
            splitLog = log.Split('~');                                                                  //predlazem da ovajamo sa '~' zato sto ':' imas u DateTime vremenu pa se buni
            if (splitLog.Count() < 3)
                return false;
            int counter = 0;                                                                             //brojac pokusaja pristupa
            int i = 0;                                                                         
            string[] split;                                                                             //niz kada splitujemo liniju iz txt fajla
            var list = new List<string>();
            var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);        //otvaranje fajla
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string line;
                if (splitLog[1].Contains("unsuccessfully"))                                           //ako u stringu koji salje FSM stoji unsuccesfully
                {
                    while ((line = streamReader.ReadLine()) != null)                                        //citanje linije po liniju
                    {
                        split = line.Split('~');                                                            //splitovanje linije iz fajla
                        if (split.Count() < 4)
                            continue;

                        if (split[0].Contains("WARNING"))
                            i = 1;
                        else
                            i = 0;

                            DateTime logTime = DateTime.Parse(split[0 + i]);                                   //izvlacim vreme loga iz txt
                            if (DateTime.Now.AddSeconds(-SysCommonObjects.N) < logTime)                                    //ako je vreme loga vece od sadasnjeg vremena minus 30 sek
                            {                                                                               //to znaci da se desilo pre manje od 30 sek i ulazi u if
                                if (split[3 + i].Contains("unsuccessfully"))                                      //ako taj log cije vreme nam odgovara sadrzi unsuccesfully
                                {
                                    if (split[4 + i].Equals(splitLog[2]))                                       //i ako je u tom logu manipulisano sa istim fajlom kao u novom logu
                                    {
                                        list.Add(line);                                                     //izvlacimo taj log (ovo je vise radi testiranja
                                        counter++;                                                           //i povecavamo brojac
                                    }
                                }
                            }
                        
                    }
                }
                else
                {
                    return false;
                }
            }
       
            if (counter >= SysCommonObjects.M - 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
     
    }
}
